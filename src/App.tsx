import { useState } from "react";
import Alert from "./components/Alert";
import Button from "./components/Button";

function App() {
  const [AlertCheck, setAlertCheck] = useState(false);
  return (
    <div>
      {AlertCheck && (
        <Alert onClose={() => setAlertCheck(false)}>My alert</Alert>
      )}
      <Button color="secondary" onclick={() => setAlertCheck(true)}>
        Hello
      </Button>
    </div>
  );
}

export default App;
