import React, { Children } from "react";
interface Prop {
  children: string;
  color?: "primary" | "secondary" | "danger";
  onclick: () => void;
}
const Button = ({ children, color, onclick }: Prop) => {
  return (
    <button className={"btn btn-" + color} onClick={onclick}>
      {children}
    </button>
  );
};

export default Button;
